class Image < ApplicationRecord
    include Rails.application.routes.url_helpers

    has_one_attached :image_url_2

    def attributes
        {
            'new_image_url' => nil
        }
    end

    def new_image_url

        image_url_2.service_url

        # polymorphic_url(image_url_2, only_path: true)

       # Rails.application.routes.url_helpers.rails_representation_url(image_url_2.variant(resize_to_limit:[200,200]).processed, only_path: true)
    end
end
