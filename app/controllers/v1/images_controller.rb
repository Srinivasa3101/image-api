class V1::ImagesController < ApplicationController
    def index
        @image = Image.all.with_attached_image_url_2.order(id: :desc)

        render json: @image, status: :ok
    end

    # def image_url_2
    #     @image = Image.find_by(id: params[:id])
    #     if @image.image_url_2.attached?
    #         redirect_to rails_blob_url(@image.image_url_2)
    #     end
    # end

    # def show
    #     @image = Image.find_by(id: params[:id])

    #     render json: @image, status: :ok
    # end

    def create
        @image = Image.new(img)

        @image.save
        render json: @image, status: :ok
    end

    private
    def img
        params.require(:image).permit(:image_url_2)
    end
end
